## Consideraciones previas ##

- Mi app usa la API de Genius (letras de canciones)

- La app está escrita en inglés por completo.

- El icono de la app y algunos iconos como las estrellas los he diseñado yo con Inkscape (programa de edición de vectores gráficos).

- Se ha seguido el modelo de desarrollo de ramas en git.

- TODO: La clave de la api se guarda cifrada en un archivo usando AES GCM sin padding. Y se desencripta para usarla.
  *No he podido hacerlo, pero los métodos de encryptar y desencryptar si están hechos y funcionan*

## Funcionamiento de la app ##

0) Comprobación si el user está logeado

    - Cuando la app comienza en la actividad main (vacía) realiza estas dos comprobaciones:

        - Si el usuario está logged (isLogged en las SharedPreferences) se pasa a la pantalla principal.
    
        - Si el usuario no está logged (isLogged en las SharedPreferences) se pasa al login.

1) Registro/login

    - El email tiene que ser válido y la pass tiene que contener al menos 8 caracteres, y al menos 1 mayúscula,
      1 minúscula, 1 dígito y un caracter especial o espacio. Por ejemplo (IES.Ayala.2021.DAM)
		
    - En ambos casos se controla que los campos estén rellenos.
	
    - En el registro, el email no puede existir ya en la base de datos (si es así, se le redirecciona al login y se rellena el email),
      si las contraseñas coinciden, el usuario es guardado en la base de datos (usando hash Argon2id para la pass, usando asynctask)
      y el usuario es logueado a la pantalla principal.
		
    - En el login, si el usuario existe en la base de datos y la contraseña es correcta, se loguea a la pantalla principal.

2) Pantalla principal (búsqueda)

    - Cualquier botón back que pulse minimiza la app, nunca vuelve a la pantalla de login/registro.
	
    - La búsqueda por defecto es "Estopa" (para que no quede vacío al entrar), se carga la lista de canciones de la API en
      un hilo secundario.
	
    - Cada canción tiene un botón (estrella) para añadir a favoritos (almacena esa cancion en la base de datos con el
      identificador del usuario que la ha guardado)

    - En la parte superior hay un menú para ir a los favoritos, en el desplegable están las preferencias y el cerrar sesión.
	
    - Se puede pinchar una canción para mostrar info adicional (detalle)
	
3) Detalle

    - Mediante el id de esa canción se llama a la api y se muestra la info de esa canción a modo de detalle.
    
    - La info se muestra en un webview que apunta a la pagina de la letra de esa canción puesto
      puesto que la API no proveía de la letra y he decidido mostrarla a través del webview.

4) Favoritos

    - Se pueden borrar favoritos con un menú de acción contextual al hacer click sobre ellos.

    - Aparece un AlertDialog para confirmar la operación y si la respuesta es positiva desaparecen del Recycler View y de
      la base de datos.
      
    - Se puede pinchar una canción para mostrar info adicional (detalle)
		
5) Preferencias

    - Se puede cambiar el tema claro/oscuro
