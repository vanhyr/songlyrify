package com.example.songlyrify.controller.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebView;

import com.example.songlyrify.R;
import com.example.songlyrify.controller.GeniusApi;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailActivity extends AppCompatActivity {

    private WebView webVwDetailLyrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Set the title of the activity
        setTitle("Song details");

        webVwDetailLyrics = findViewById(R.id.webVwDetailLyrics);

        // Get the intent data
        Intent intentHome = getIntent();
        // Making sure the register intent returns the extras
        if (intentHome.hasExtra("song_id")) {
            // Make the request to the api
            new DetailActivity.ApiCall().execute("GET", intentHome.getStringExtra("song_id"));
        }

    }

    /**
     * Do the Api call in an AsyncTask because the call is time consuming and depends on server
     * response.
     * Leaks might occur if it is not static, but can't access some things if it isn't.
     */
    @SuppressLint("StaticFieldLeak")
    private class ApiCall extends AsyncTask<String,Void,String> {

        ApiCall() {
            super();
        }

        @Override
        protected String doInBackground(@NonNull String... strings) {
            String response = null;
            // GET/POST
            String method = strings[0];
            // The idx
            int idx = Integer.parseInt(strings[1]);
            switch (method) {
                case "GET":
                    // Search the song in the API by the id
                    response = new GeniusApi().getSong(idx);
                    break;
                case "POST":
                    // TODO
                    break;
            }
            return response;
        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        protected void onPostExecute(String response) {
            try {
                // Check if any response is given by the API REST
                if (response != null) {
                    // Navigate through the json and get the data we need as a json array (the hits/songs)
                    JSONObject jsonSong = new JSONObject(response).getJSONObject("response").getJSONObject("song");
                    String lyricsUrl = jsonSong.getString("url");
                    webVwDetailLyrics.loadUrl(lyricsUrl);
                    // Enabling javascript is dangerous but it might be necessary in order to show the url
                    //webVwDetailLyrics.getSettings().setJavaScriptEnabled(true);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

    }

}