package com.example.songlyrify.controller.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.songlyrify.R;
import com.example.songlyrify.controller.adapter.HomeRecyclerAdapter;
import com.example.songlyrify.controller.GeniusApi;
import com.example.songlyrify.model.Song;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Home activity, for showing Genius API search results for the given search (songs)
 *
 * @author Valentin Ros Duart.
 */
public class HomeActivity extends AppCompatActivity {

    private SearchView searchVwQuery;
    private RecyclerView rclrVwHome;
    private HomeRecyclerAdapter homeRecyclerAdapter;

    private List<Song> songs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Set the title of the activity
        setTitle("Search");

        searchVwQuery = findViewById(R.id.searchVwQuery);
        rclrVwHome = findViewById(R.id.rclrVwHome);

        // Set the text listener to the search view
        searchVwQuery.setOnQueryTextListener(search);
        // Set the default search results (in order to display data on start)
        searchVwQuery.setQuery("Estopa", true);

        // Initialize the song list empty
        songs = new ArrayList<>();

        // Pass the song list to the recycler adapter
        homeRecyclerAdapter = new HomeRecyclerAdapter(songs);

        // Set the recycler view custom adapter we created
        rclrVwHome.setAdapter(homeRecyclerAdapter);
        // Set the layout to linear (although grid could of been used for example)
        rclrVwHome.setLayoutManager(new LinearLayoutManager(this));

        // Set the default search results (in order to display data on start)
        //new ApiCall().execute("GET", "Estopa");
    }

    /**
     * QueryTextListener function.
     * Search on the API on every text change and submit.
     */
    private final SearchView.OnQueryTextListener search = new SearchView.OnQueryTextListener() {

        /**
         * Performs a search in the API for the given query every time text changes.
         * @param query the query.
         * @return true.
         */
        @Override
        public boolean onQueryTextChange(String query) {
            // Make an api call for the given query (doesn't work well cause it is slow searching)
            //new ApiCall().execute("GET", query);
            return true;
        }

        /**
         * Performs a search in the API for the given query when the user submits the search.
         * @param query the query.
         * @return true.
         */
        @Override
        public boolean onQueryTextSubmit(String query) {
            // Make an api call for the given query
            new ApiCall().execute("GET", query);
            return true;
        }

    };

    /*// Idk why this ain't working
    private final SearchView.OnQueryTextListener search = () -> {

        @Override
        public boolean onQueryTextChange(String query) {
            //new ApiCall().execute("GET", query);
            return true;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            // Make an api call for the given query, and perform the search
            new ApiCall().execute("GET", query);
            return true;
        }

    };*/

    /**
     * Set a custom menu.
     * @param menu the default menu.
     * @return true.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    /**
     * Handle menu events.
     * @param item the menu item.
     * @return true.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        final int favsItemId = R.id.favs_item;
        final int settingsItemId = R.id.settings_item;
        final int logoutItemId = R.id.logout_item;
        switch (itemId) {
            case favsItemId:
                startActivity(new Intent(HomeActivity.this, FavsActivity.class));
                break;
            case settingsItemId:
                startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
                break;
            case logoutItemId:
                // Get the shared session preferences
                SharedPreferences sharedPreferences = getSharedPreferences("Session", MODE_PRIVATE);
                // Creating an Editor object to edit (write to the file)
                SharedPreferences.Editor spEditor = sharedPreferences.edit();
                // Set email to empty and isLogged to false
                spEditor.putString("email", "");
                spEditor.putBoolean("isLogged", false);
                // Apply edits
                spEditor.apply();
                // Go to login
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                break;
        }
        return true;
    }

    /**
     * Prevents main/home screen from going back to login/register screen when back button
     * is pressed.
     *
     */
    @Override
    public void onBackPressed() {
        // Emulates home button (minimizes the app)
        moveTaskToBack(true);
    }

    /**
     * Do the Api call in an AsyncTask because the call is time consuming and depends on server
     * response.
     * Leaks might occur if it is not static, but can't access some things if it isn't.
     */
    @SuppressLint("StaticFieldLeak")
    private class ApiCall extends AsyncTask<String,Void,String> {

        ApiCall() {
            super();
        }

        @Override
        protected String doInBackground(@NonNull String... strings) {
            String response = null;
            // GET/POST
            String method = strings[0];
            // The query
            String query = strings[1];
            switch (method) {
                case "GET":
                    // Search in the API by the given query
                    response = new GeniusApi().search(query);
                    break;
                case "POST":
                    // TODO
                    break;
            }
            return response;
        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        protected void onPostExecute(String response) {
            try {
                // Check if any response is given by the API REST
                if (response != null) {
                    // Clear the list
                    songs.clear();
                    // Navigate through the json and get the data we need as a json array (the hits/songs)
                    JSONArray hits = new JSONObject(response).getJSONObject("response").getJSONArray("hits");
                    int id;
                    String title, artist, imageUrl;
                    // For every hit/song get the info we need and create a Song with that,
                    // then add it to our songs List
                    for (int i = 0; i < hits.length(); i++) {
                        id = Integer.parseInt(hits.getJSONObject(i).getJSONObject("result").getString("id"));
                        title = hits.getJSONObject(i).getJSONObject("result").getString("title");
                        artist = hits.getJSONObject(i).getJSONObject("result").getString("artist_names");
                        imageUrl = hits.getJSONObject(i).getJSONObject("result").getString("song_art_image_url");
                        songs.add(new Song(id, title, artist, imageUrl));
                    }
                    // Tell the recycler adapter we changed the data
                    homeRecyclerAdapter.notifyDataSetChanged();
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

    }

}