package com.example.songlyrify.controller.fragments;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.example.songlyrify.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
    }

}