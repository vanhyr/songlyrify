package com.example.songlyrify.controller.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.songlyrify.R;
import com.example.songlyrify.model.User;
import com.example.songlyrify.controller.Cryptography;
import com.example.songlyrify.util.PopUp;
import com.example.songlyrify.util.Validator;
import com.google.android.material.textfield.TextInputLayout;
import com.orm.SugarContext;

/**
 * Login activity, for managing users login using db (sugar)
 *
 * @author Valentin Ros Duart.
 */
public class LoginActivity extends AppCompatActivity {

    private EditText edtEmailLogin, edtPassLogin;
    private TextInputLayout tilEmailLogin, tilPassLogin;
    private TextView txtVwRegisterLink;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Find UI elements by id
        tilEmailLogin = findViewById(R.id.tilEmailLogin);
        tilPassLogin = findViewById(R.id.tilPassLogin);

        edtEmailLogin = findViewById(R.id.edtEmailLogin);
        edtPassLogin = findViewById(R.id.edtPassLogin);

        txtVwRegisterLink = findViewById(R.id.txtVwRegisterLink);

        btnLogin = findViewById(R.id.btnLogin);

        // Add listeners
        txtVwRegisterLink.setOnClickListener(goRegister);
        btnLogin.setOnClickListener(login);

        // Set the title of the activity
        setTitle("Log In");

        // Get the intent data
        Intent intentRegister = getIntent();
        // Making sure the register intent returns the extras
        if (intentRegister.hasExtra("msg") && intentRegister.hasExtra("email")) {
            // Set the email to the one used during registration, cause it already exists in db
            edtEmailLogin.setText(intentRegister.getStringExtra("email"));
            // Show a toast telling the user to login
            PopUp.showShortToastyInfo(this, intentRegister.getStringExtra("msg"));
            // Focus the pass field
            tilPassLogin.requestFocus();
        }

    }

    /**
     * OnClickListener arrow function that sends the user to the register view.
     * Triggers when txtVwRegisterLink is pressed.
     */
    private final View.OnClickListener goRegister = view -> {
        // Go to register view
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    };

    /**
     * OnClickListener arrow function that logins the user and sends him to home view.
     * Triggers when btnLogin is pressed. If the user exists, logs the user in (goes to home page)
     */
    private final View.OnClickListener login = view -> {
        // Clear error messages
        tilEmailLogin.setError(null);
        tilPassLogin.setError(null);
        // Get input data
        String inputEmail = edtEmailLogin.getText().toString();
        String inputPass = edtPassLogin.getText().toString();
        // Check if input data is valid, if it is valid then login
        if (Validator.isValidEmail(inputEmail) && Validator.isValidPass(inputPass)) {
            // Start and create db connection
            SugarContext.init(this);
            // Check if email exists in the db
            if (User.find(User.class, "email = ?", inputEmail).size() > 0) {
                // Get the user for that email
                User dbUser = User.find(User.class, "email = ?", inputEmail).get(0);
                // Using an AsyncTask for checking hash (might be time consuming)
                CheckHash checkHash = new CheckHash();
                /* NOTE:
                    I'm calling the doInBackground method (idk if it is correct) because
                    I can't use startActivity inside a static class (CheckHash -> onPostExecute)
                    and CheckHash needs to be static in order to prevent leaks.
                */
                // Check if input pass matches the hash and salt from the db
                if (checkHash.doInBackground(inputPass, dbUser.getPassSalt(), dbUser.getPassHash())) {
                    PopUp.showShortToastySuccess(this, "Successfully logged in.");
                    // Close db connection before changing the activity
                    SugarContext.terminate();
                    // Storing data into SharedPreferences
                    SharedPreferences sharedPreferences = getSharedPreferences("Session", MODE_PRIVATE);
                    // Creating an Editor object to edit (write to the file)
                    SharedPreferences.Editor spEditor = sharedPreferences.edit();
                    // Storing the key and its value as the data fetched from edittext
                    spEditor.putString("email", inputEmail);
                    spEditor.putBoolean("isLogged", true);
                    spEditor.commit();
                    // Go to home page
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    // Didn't match, show error and focus the password field
                } else {
                    tilPassLogin.setError("Wrong password, try again.");
                    tilPassLogin.requestFocus();
                }
            // Email doesn't exists in the db, show error and focus the email field
            } else {
                tilEmailLogin.setError("Email not found, please try again or register.");
                tilEmailLogin.requestFocus();
            }
            // Close db connection
            SugarContext.terminate();
        // Input data is not valid, show toast and focus the email field
        } else {
            PopUp.showShortToastyError(this, "Input data is wrong, please, consider checking values");
            tilEmailLogin.requestFocus();
        }
    };

    /**
     * Do the hash check in an AsyncTask because the hashing algorithm I used (Argon2Id) might
     * be time consuming.
     */
    private static class CheckHash extends AsyncTask<String, Void, Boolean> {

        CheckHash() {
            super();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            String inputPass = strings[0];
            String passSalt = strings[1];
            String passHash = strings[2];
            return Cryptography.checkHashArgon2Id(inputPass, passSalt, passHash);
        }

    }

}

