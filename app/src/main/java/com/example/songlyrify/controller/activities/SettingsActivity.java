package com.example.songlyrify.controller.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.songlyrify.R;
import com.example.songlyrify.controller.fragments.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Set the title of the activity
        setTitle("Settings");

        // Create the fragment
        getSupportFragmentManager()
                .beginTransaction()
                // Put the settings into the given container
                .replace(R.id.settings_container, new SettingsFragment())
                .commit();
        // Enable the go back button/icon
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            // If exists, show the back button
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Get the shared preferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
        // Add the on shared preference change listener
        sharedPreferences.registerOnSharedPreferenceChangeListener(spChanged);
    }

    /**
     * Shared Preference change listener handler.
     * Triggers when a shared preference is changed.
     */
    SharedPreferences.OnSharedPreferenceChangeListener spChanged = (sharedPreferences, key) -> {
        switch (key) {
            case "dark_mode":
                if (sharedPreferences.getBoolean(key,false)) {
                    // Set theme to night mode
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                } else {
                    // Set theme to light mode
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                break;
        }
    };

    // When back is pressed
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}