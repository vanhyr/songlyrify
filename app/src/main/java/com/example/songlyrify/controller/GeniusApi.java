package com.example.songlyrify.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Class for managing Genius Api calls.
 *
 * @author Valentin Ros Duart.
 */
public class GeniusApi {

    private static final String ENDPOINT = "https://api.genius.com/";
    // TODO: Put this into a separate file with aes encryption and decrypt before using it
    private static final String TOKEN = "CfPjgRM09PliGCkWlO33eseAu4oCTbr6_ZeqaVZj5ExZPPmTXeiU-NMXDwPakIPF";

    private final ApiRest apiRest;

    public GeniusApi() {
        apiRest = new ApiRest(ENDPOINT, TOKEN);
    }

    /**
     * Searches a song/artist songs using the Genius REST API.
     * @param query the song/artist name.
     * @return the result.
     */
    public String search(String query) {
        String encodedQuery;
        try {
            // Encode to match url format standards
            encodedQuery = URLEncoder.encode(query, StandardCharsets.UTF_8.name());
            return apiRest.getRequest("search?q=" + encodedQuery);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     * Get a song using the Genius REST API.
     * @param id the song id.
     * @return the result.
     */
    public String getSong(int id) {
        String encodedQuery;
        try {
            // Encode to match url format standards
            encodedQuery = URLEncoder.encode(String.valueOf(id), StandardCharsets.UTF_8.name());
            return apiRest.getRequest("songs/" + encodedQuery);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return "";
        }
    }

}
