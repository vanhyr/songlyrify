package com.example.songlyrify.model;

import androidx.annotation.NonNull;

import com.orm.SugarRecord;

/**
 * Model class for a user using Sugar.
 * The library, will create a primary key column called id that auto-increments by default.
 * @author Valentin Ros Duart.
 */
public class User extends SugarRecord {

    private String email, passSalt, passHash;

    /**
     * Default constructor.
     * Needs to be declared for Sugar to create the auto increment id (primary key)
     */
    public User() {

    }

    /**
     * Constructor.
     * @param email the email.
     * @param passSalt the pass hash salt.
     * @param passHash the pass hash value.
     */
    public User(String email, String passSalt, String passHash) {
        this.email = email;
        this.passSalt = passSalt;
        this.passHash = passHash;
    }

    /**
     * Retrieve the email.
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Retrieve the pass hash salt.
     * @return the pass hash salt.
     */
    public String getPassSalt() {
        return passSalt;
    }

    /**
     * Retrieve the pass hash value.
     * @return the pass hash value.
     */
    public String getPassHash() {
        return passHash;
    }

    /**
     * Set the email.
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Set the pass hash salt.
     * @param passSalt the pass hash salt.
     */
    public void setPassSalt(String passSalt) {
        this.passSalt = passSalt;
    }

    /**
     * Set the pass hash value.
     * @param passHash the pass hashed value.
     */
    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    /**
     * UserEncrypt object to String.
     * @return the string.
     */
    @NonNull
    @Override
    public String toString() {
        return "UserEncrypt{" + "email= " + email + ", passSalt= " + passSalt + ", passHash= " + passHash + "}";
    }

}
