package com.example.songlyrify.model;

import androidx.annotation.NonNull;

import com.orm.SugarRecord;

/**
 * Model class for a song using Sugar.
 * The library, will create a primary key column called id that auto-increments by default.
 * @author Valentin Ros Duart.
 */
public class Song extends SugarRecord {

    // id is renamed to idx because may clash with sugar id
    private int idx;
    private String title, artist, imageUrl, userEmail;

    /**
     * Default constructor.
     * Needs to be declared for Sugar to create the auto increment id (primary key)
     */
    public Song() {

    }

    public Song(int idx, String title, String artist, String imageUrl) {
        this.idx = idx;
        this.title = title;
        this.artist = artist;
        this.imageUrl = imageUrl;
    }

    public Song(int idx, String title, String artist, String imageUrl, String userEmail) {
        this.idx = idx;
        this.title = title;
        this.artist = artist;
        this.imageUrl = imageUrl;
        this.userEmail = userEmail;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @NonNull
    @Override
    public String toString() {
        return "Song{" +
                "idx='" + idx + '\'' +
                ", title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", userEmail='" + userEmail + '\'' +
                '}';
    }

}
