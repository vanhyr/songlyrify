package com.example.songlyrify.util;

import android.text.TextUtils;

import java.util.regex.Pattern;

/**
 * Class for validating data (usually from user input)
 */
public class Validator {

    /**
     * Checks if the given email is valid or not. Format is: username@domain.extension.
     * (valid ex: vrosdua059@iesayala.es)
     * @param email the email address
     * @return true if it's valid, false if not.
     */
    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Checks if the given pass is valid or not. Must contain minimum 8 characters and
     * at least 1 upper case letter, 1 lower case letter, 1 digit and 1 special character or space.
     * (valid ex: IES.Ayala.2021.DAM)
     * @param pass the password
     * @return true if it's valid, false if not.
     */
    public static boolean isValidPass(String pass) {
        Pattern PASSWORD = Pattern.compile("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*.-]).{8,}$");
        return !TextUtils.isEmpty(pass) && PASSWORD.matcher(pass).matches();
    }

}
